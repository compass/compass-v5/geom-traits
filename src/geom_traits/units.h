#pragma once

#include <cmath>

namespace geom {
// we allow operations between vertices (i.e. array)
using namespace compass::utils::array_operators;
using Scalar = double;

template <typename Vertex>
static inline Scalar distance(Vertex &v0, Vertex &v1) {
  Scalar dist = 0.0;
  auto vect = v1 - v0;
  for (auto &&v : vect)
    dist += std::pow(v, 2);
  return sqrt(dist);
}

template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 1, Scalar>
triangle_area(const Vertex &a, const Vertex &b, const Vertex &c) {
  return 0.0;
}

template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 2, Scalar>
triangle_area(const Vertex &a, const Vertex &b, const Vertex &c) {
  // norm(vector product) / 2
  Vertex ab = b - a;
  Vertex ac = c - a;
  return abs((ab[0] * ac[1] - ab[1] * ac[0])) / 2.0;
}

template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 3, Scalar>
triangle_area(const Vertex &a, const Vertex &b, const Vertex &c) {
  // norm(vector product) / 2
  Vertex ab = b - a;
  Vertex ac = c - a;
  return sqrt(std::pow(ab[1] * ac[2] - ab[2] * ac[1], 2) +
              std::pow(ab[2] * ac[0] - ab[0] * ac[2], 2) +
              std::pow(ab[0] * ac[1] - ab[1] * ac[0], 2)) /
         2.0;
}

template <typename Vertex>
static inline Scalar tetra_volume(const Vertex &a, const Vertex &b,
                                  const Vertex &c, const Vertex &d) {
  auto xT = a + b + c + d;
  xT /= 4.0;
  auto e0 = xT - d;
  auto e1 = a - d;
  auto e2 = b - d;
  auto e3 = c - d;

  auto volT = e1[0] * e2[1] * e3[2] + e2[0] * e3[1] * e1[2] +
              e3[0] * e1[1] * e2[2] - e1[0] * e2[2] * e3[1] -
              e2[0] * e3[2] * e1[1] - e3[0] * e1[2] * e2[1];
  return abs(volT) / 6.0;
}

// calcul of the outward unit normal vector v of the triangle a, b, c
// outward wrt the point x, y, z > vx, vy, vz
// and calcul of the surface of the triangle
template <typename Vertex>
static inline auto triangle_VecNormalT(const Vertex &a, const Vertex &b,
                                       const Vertex &c, const Vertex &x) {

  assert(a.size() == 3 && "triangle_VecNormalT not implemented for other dim");

  Vertex xt = (a + b + c);
  xt /= 3.0;

  Vertex ab = b - a;
  Vertex ac = c - a;
  Vertex v;
  v[0] = ac[2] * ab[1] - ac[1] * ab[2];
  v[1] = ac[0] * ab[2] - ac[2] * ab[0];
  v[2] = ac[1] * ab[0] - ac[0] * ab[1];

  // norm of v
  auto surf = sqrt(std::pow(v[0], 2) + std::pow(v[1], 2) + std::pow(v[2], 2));

  // unit vector
  v /= surf;
  // surface = norm of v / 2
  surf /= 2.0;

  // scalar product between (xt-x) and v to get the sign
  auto s =
      (xt[0] - x[0]) * v[0] + (xt[1] - x[1]) * v[1] + (xt[2] - x[2]) * v[2];

  if (s < 0.0)
    v *= -1.0;

  return std::make_tuple(v, surf);
}

// calcul of the unit normal vector v of the triangle a, b, c
template <size_t n, typename Vertex>
static inline std::enable_if_t<n == 3, Vertex>
normal_vect(const Vertex &a, const Vertex &b, const Vertex &c) {
  Vertex xt = (a + b + c);
  xt /= 3.0;

  Vertex ab = b - a;
  Vertex ac = c - a;
  Vertex v;
  v[0] = ac[2] * ab[1] - ac[1] * ab[2];
  v[1] = ac[0] * ab[2] - ac[2] * ab[0];
  v[2] = ac[1] * ab[0] - ac[0] * ab[1];

  // norm of v
  auto surf = sqrt(std::pow(v[0], 2) + std::pow(v[1], 2) + std::pow(v[2], 2));
  // unit vector
  v /= surf;

  return v;
}

} // namespace geom
