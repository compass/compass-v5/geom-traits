import numpy as np
from icus.fig import Zone


def grid_center(geom):
    grid = geom.mesh
    return tuple(orig + 0.5 * l for [orig, l] in zip(grid.origin, grid.extent))


def below(axis, lim):
    return lambda pts: pts[:, axis] <= lim


def above(axis, lim):
    return lambda pts: pts[:, axis] >= lim


def on_xvalue(x):
    return lambda pts: pts[:, 0] == x


def on_yvalue(y):
    return lambda pts: pts[:, 1] == y


def on_zvalue(z):
    return lambda pts: pts[:, 2] == z


def below_axis_limit(grid, axis, epsilon=0):
    return below(axis, grid.origin[axis] + epsilon)


def above_axis_limit(grid, axis, epsilon=0):
    return above(axis, grid.origin[axis] + grid.extent[axis] - epsilon)


def on_xmin(grid, epsilon=0):
    return below_axis_limit(grid, 0, epsilon)


def on_xmax(grid, epsilon=0):
    return above_axis_limit(grid, 0, epsilon)


def on_ymin(grid, epsilon=0):
    return below_axis_limit(grid, 1, epsilon)


def on_ymax(grid, epsilon=0):
    return above_axis_limit(grid, 1, epsilon)


def on_zmin(grid, epsilon=0):
    return below_axis_limit(grid, 2, epsilon)


def on_zmax(grid, epsilon=0):
    return above_axis_limit(grid, 2, epsilon)


def on_vertical_boundaries(grid, epsilon=0):
    return (
        lambda pts: on_xmin(grid, epsilon)(pts)
        | on_xmax(grid, epsilon)(pts)
        | on_ymin(grid, epsilon)(pts)
        | on_ymax(grid, epsilon)(pts)
    )


def on_any_boundary(grid, epsilon=0):
    return (
        lambda pts: on_vertical_boundaries(grid, epsilon)(pts)
        | on_zmin(grid, epsilon)(pts)
        | on_zmax(grid, epsilon)(pts)
    )


def bound_f_factory(elemts_pos, elemts_iset, bound_condition):
    def func(geom, epsilon=0):
        pos = elemts_pos(geom).as_array()
        return Zone(elemts_iset(geom).extract(bound_condition(geom.mesh, epsilon)(pos)))

    return func


# define top_boundary or top_boundary_nodes or top_boundary_faces...
def _define_all_func():
    places = {
        "vertical_boundary": on_vertical_boundaries,
        "xmin_boundary": on_xmin,
        "xmax_boundary": on_xmax,
        "ymin_boundary": on_ymin,
        "ymax_boundary": on_ymax,
        "top_boundary": on_zmax,
        "bottom_boundary": on_zmin,
        "all_boundaries": on_any_boundary,
    }
    elemts_name = {
        "_nodes": [lambda geom: geom.mesh.vertices, lambda geom: geom.mesh.nodes],
        "_edges": [
            lambda geom: geom.edge_centers,
            lambda geom: geom.mesh.edges,
        ],
        "_faces": [
            lambda geom: geom.face_centers,
            lambda geom: geom.mesh.faces,
        ],
        # all elements (whatever object of the mesh close to the concerned coordinates)
        "": [
            lambda geom: geom.get_iset_centers(geom.mesh.joined_elements),
            lambda geom: geom.mesh.joined_elements,
        ],
    }

    for place, func_p in places.items():
        for elemts, attr in elemts_name.items():
            globals()[f"{place}{elemts}"] = bound_f_factory(*attr, func_p)


_define_all_func()
