import verstr

try:
    from . import _version

    __version__ = verstr.verstr(_version.version)
except ImportError:
    __version__ = None

# all bindings are defined in a single compiled module
# this is not the only possible strategy...
# adapt the current file to your needs

import compass

with compass.nanobind():
    from geom_traits.bindings import *

from geom_traits.grid import *
