#pragma once

#include <cmath>
#include <compass_cxx_utils/array_utils.h>
#include <compass_cxx_utils/std_ranges.h>
#include <geom_traits/units.h>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <vector>

namespace geom {
// we allow operations between vertices (i.e. array)
using namespace compass::utils::array_operators;
using compass::std_ranges::views::zip;

// contains methods to compute centers, measures...
template <typename mesh_t> struct GeomProperties {
  using Vertex = mesh_t::vertex_type;
  using Vertex_field = icus::Field<Vertex>;
  using Scalar_field = icus::Field<Scalar>;
  static constexpr std::size_t dimension = mesh_t::dimension;
  mesh_t mesh; // copy is light
  std::array<Vertex_field, dimension + 1> centers;
  std::array<Scalar_field, dimension + 1> measures;
  GeomProperties(mesh_t &the_mesh) : mesh{the_mesh} {}
  const mesh_t &get_mesh() const { return mesh; }

  template <std::size_t obj_dim> Vertex_field &get_centers() {
    static_assert(obj_dim >= 0 && obj_dim <= mesh.cell);
    if (!centers[obj_dim].values) {
      auto ok = _fill_centers<obj_dim>();
      assert(ok);
    }
    return centers[obj_dim];
  }

  Vertex_field &get_cell_centers() { return get_centers<mesh_t::cell>(); }
  Vertex_field &get_face_centers() { return get_centers<mesh_t::face>(); }
  Vertex_field &get_edge_centers() { return get_centers<mesh_t::edge>(); }
  Vertex_field &get_node_centers() { return get_centers<mesh_t::node>(); }

  Vertex_field get_iset_centers(const icus::ISet &iset) {
    auto iset_centers = Vertex_field{iset};
    _fill_iset_centers<mesh_t::node>(iset, iset_centers);
    _fill_iset_centers<mesh_t::edge>(iset, iset_centers);
    // if dim > 1, cells are other objects (nor nodes nor edges)
    if constexpr (dimension > 1) {
      _fill_iset_centers<mesh_t::cell>(iset, iset_centers);
      // if dim > 2, faces are other objects (nor nodes, nor edges, nor cells)
      if constexpr (dimension > 2)
        _fill_iset_centers<mesh_t::face>(iset, iset_centers);
    }
    return iset_centers;
  }

  template <std::size_t obj_dim>
  void _fill_iset_centers(icus::ISet iset, Vertex_field iset_centers) {
    // intersect iset and mesh object where new_base is iset
    auto obj_iset = icus::intersect(iset, mesh.elements[obj_dim], iset);
    if (obj_iset.size() > 0) {
      auto obj_centers = get_centers<obj_dim>().extract(obj_iset);
      // obj_iset.mapping is the index in iset
      for (auto &&[i, P] : zip(obj_iset.mapping(), obj_centers.span()))
        iset_centers(i) = P;
    }
  }

  // *measures* (=length, area, volume)
  // by convention the measure of dimension 0 object is 1
  template <std::size_t obj_dim> Scalar_field &get_measures() {
    assert(obj_dim >= 0 && obj_dim <= mesh.cell);
    if (!measures[obj_dim].values) {
      auto ok = _fill_measures<obj_dim>();
      assert(ok);
    }
    return measures[obj_dim];
  }

  Scalar_field &get_cell_measures() { return get_measures<mesh_t::cell>(); }
  Scalar_field &get_face_measures() { return get_measures<mesh_t::face>(); }
  Scalar_field &get_edge_measures() { return get_measures<mesh_t::edge>(); }
  Scalar_field &get_node_measures() { return get_measures<mesh_t::node>(); }

  Scalar_field get_iset_measures(icus::ISet iset) {
    auto iset_measures = Scalar_field{iset};
    _fill_iset_measures<mesh_t::node>(iset, iset_measures);
    _fill_iset_measures<mesh_t::edge>(iset, iset_measures);
    // if dim > 1, cells are other objects (nor nodes nor edges)
    if constexpr (dimension > 1) {
      _fill_iset_measures<mesh_t::cell>(iset, iset_measures);
      // if dim > 2, faces are other objects (nor nodes, nor edges, nor cells)
      if constexpr (dimension > 2)
        _fill_iset_measures<mesh_t::face>(iset, iset_measures);
    }
    return iset_measures;
  }

  template <std::size_t obj_dim>
  void _fill_iset_measures(icus::ISet iset, Scalar_field iset_measures) {
    // intersect iset and mesh object where new_base is iset
    auto obj_iset = icus::intersect(iset, mesh.elements[obj_dim], iset);
    if (obj_iset.size() > 0) {
      auto obj_measures = get_measures<obj_dim>().extract(obj_iset);
      // obj_iset.mapping is the index in iset
      for (auto &&[i, meas] : zip(obj_iset.mapping(), obj_measures.span()))
        iset_measures(i) = meas;
    }
  }

  template <std::size_t obj_dim> bool _fill_centers() {
    static_assert(obj_dim >= 0);
    if constexpr (obj_dim == mesh_t::node) {
      centers[obj_dim] = mesh.vertices;
    } else if constexpr (obj_dim <= dimension) {
      centers[obj_dim] =
          barycenter(mesh.template get_connectivity<obj_dim, mesh_t::node>(),
                     mesh.vertices);
    } else {
      return false;
    }
    return true;
  }

  template <std::size_t obj_dim> bool _fill_measures() {
    static_assert(obj_dim >= 0 && obj_dim <= dimension);
    if constexpr (obj_dim == 0) {
      measures[obj_dim] = Scalar_field{mesh.elements[obj_dim]};
      // by convention the measure of dimension 0 object is 1
      measures[obj_dim] = 1.0;
    } else if constexpr (obj_dim == 1) {
      measures[obj_dim] =
          length(mesh.template get_connectivity<obj_dim, mesh_t::node>(),
                 mesh.vertices);
    } else if constexpr (obj_dim == 2) {
      measures[obj_dim] = area(
          mesh.template get_connectivity<obj_dim, mesh_t::edge>(), // obj - edge
          mesh.template get_connectivity<mesh_t::edge,
                                         mesh_t::node>(), // edge - node
          mesh.vertices, get_centers<obj_dim>());
    } else if constexpr (obj_dim == 3) {
      measures[obj_dim] =
          volume(mesh.template get_connectivity<mesh_t::cell, mesh_t::face>(),
                 mesh.template get_connectivity<mesh_t::face, mesh_t::edge>(),
                 mesh.template get_connectivity<mesh_t::edge, mesh_t::node>(),
                 get_cell_centers(), get_face_centers(), mesh.vertices);
    } else {
      return false;
    }
    return true;
  }

  template <typename Connectivity>
  static Vertex_field barycenter(Connectivity &connectivity,
                                 const Vertex_field &target_coords) {
    assert(are_same_indexed_set(connectivity.target(), target_coords.support));
    auto prop_centers = Vertex_field{connectivity.source()};
    for (auto &&[s, targets] : connectivity.targets_by_source())
      prop_centers(s) = one_barycenter(targets, target_coords);
    return prop_centers;
  }

  static inline Vertex one_barycenter(const auto &targets,
                                      const Vertex_field &target_coords) {
    auto bary = Vertex{};
    for (auto &&t : targets)
      bary += target_coords(t);
    bary /= Scalar(targets.size());
    return bary;
  }

  template <typename Connectivity>
  static Scalar_field length(Connectivity &connectivity,
                             const Vertex_field &target_coords) {
    assert(are_same_indexed_set(connectivity.target(), target_coords.support));
    auto prop_length = Scalar_field{connectivity.source()};
    for (auto &&[s, targets] : connectivity.targets_by_source()) {
      assert(targets.size() == 2);
      // FIXME todo
      prop_length(s) = geom::distance(target_coords(*targets.begin()),
                                      target_coords(*(targets.begin() + 1)));
    }
    return prop_length;
  }

  template <typename Connectivity0, typename Connectivity1>
  static Scalar_field
  area(Connectivity0 &surf_edges, Connectivity1 &edges_nodes,
       const Vertex_field &target_coords, const Vertex_field &source_centers) {
    assert(are_same_indexed_set(edges_nodes.target(), target_coords.support));
    assert(are_same_indexed_set(surf_edges.source(), source_centers.support));
    auto prop_area = Scalar_field{surf_edges.source()};
    for (auto &&[s, edges] : surf_edges.targets_by_source()) {
      assert(edges.size() > 2);
      prop_area(s) = 0.0;
      // cut the 2D object in triangles using the center
      for (auto &&the_edge : edges) {
        auto &&[n0, n1] = edges_nodes.targets_by_source(the_edge);
        prop_area(s) += triangle_area<dimension>(
            target_coords(n0), target_coords(n1), source_centers(s));
      }
    }
    return prop_area;
  }

  template <typename Connectivity0, typename Connectivity1,
            typename Connectivity2>
  static Scalar_field
  volume(Connectivity0 &cell_face_conn, Connectivity1 &face_edge_conn,
         Connectivity2 &edge_node_conn, const Vertex_field &cell_centers,
         const Vertex_field &face_centers, const Vertex_field &vert_coords) {
    assert(dimension == 3);
    assert(are_same_indexed_set(cell_face_conn.source(), cell_centers.support));
    assert(
        are_same_indexed_set(cell_face_conn.target(), face_edge_conn.source()));
    assert(are_same_indexed_set(face_edge_conn.source(), face_centers.support));
    assert(
        are_same_indexed_set(face_edge_conn.target(), edge_node_conn.source()));
    assert(are_same_indexed_set(edge_node_conn.target(), vert_coords.support));

    auto prop_vol = Scalar_field{cell_face_conn.source()};
    for (auto &&[the_cell, faces] : cell_face_conn.targets_by_source()) {
      prop_vol(the_cell) = 0.0;
      auto &&yk = cell_centers(the_cell);
      for (auto &&the_face : faces) {
        auto &&xs = face_centers(the_face);

        for (auto &&the_edge : face_edge_conn.targets_by_source(the_face)) {
          auto &&[n0, n1] = edge_node_conn.targets_by_source(the_edge);
          auto &&x0 = vert_coords(n0);
          auto &&x1 = vert_coords(n1);

          prop_vol(the_cell) += tetra_volume(x0, x1, xs, yk);
        }
      }
    }
    return prop_vol;
  }

  template <size_t n, typename Vertex>
  inline std::enable_if_t<n == 3, Vertex>
  face_unit_normal(const auto &f_nodes) {
    // get 3 nodes belonging to the face f, true only if dim > 2 !
    auto x0 = mesh.vertices(f_nodes[0]);
    auto x1 = mesh.vertices(f_nodes[1]);
    auto x2 = mesh.vertices(f_nodes[2]);
    return normal_vect<n>(x0, x1, x2);
  }
};

} // namespace geom
