#include <geom_traits/geom-traits.h>
#include <icmesh/Mesh.h>
#include <icus/ISet.h>
#include <nanobind/nanobind.h>
#include <nanobind_utils/nanobind-utils.h>

namespace nb = nanobind;
namespace cnb = compass::nanobind;

template <typename mesh_t>
void bind_geom_traits(nb::module_ &module, auto &class_name) {
  using GeomP = geom::GeomProperties<mesh_t>;
  auto cls =
      nb::class_<GeomP>(module, class_name, "Useful calculus over geometry");
  cls.def(nb::init<const GeomP &>());
  cls.def(nb::init<mesh_t &>());
  cls.def_ro("mesh", &GeomP::mesh);

  // build the centers of an iset
  cls.def("get_iset_centers", &GeomP::get_iset_centers);
  cls.def_prop_ro("cell_centers", &GeomP::get_cell_centers);
  cls.def_prop_ro("face_centers", &GeomP::get_face_centers);
  cls.def_prop_ro("edge_centers", &GeomP::get_edge_centers);
  // build the measures of an iset
  cls.def("get_iset_measures", &GeomP::get_iset_measures);
  cls.def_prop_ro("cell_measures", &GeomP::get_cell_measures);
  cls.def_prop_ro("face_measures", &GeomP::get_face_measures);
  cls.def_prop_ro("edge_measures", &GeomP::get_edge_measures);

  module.def("make_geom", [](mesh_t &the_mesh) { return GeomP(the_mesh); });
}

template <std::size_t dim> void bind_struct_geom_traits(nb::module_ &module) {
  using mesh_s = icmesh::Mesh<dim, icmesh::structured_connectivity_matrix<dim>>;
  auto cls_name_str =
      std::string("StructGeomProperties_") + std::to_string(dim);
  auto cls_name = cls_name_str.c_str();
  bind_geom_traits<mesh_s>(module, cls_name);
}

NB_MODULE(bindings, module) {
  nb::module_::import_("icmesh"); // for get_mesh

  bind_struct_geom_traits<1>(module);
  bind_struct_geom_traits<2>(module);
  bind_struct_geom_traits<3>(module);
}
