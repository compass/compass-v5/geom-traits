#include <geom_traits/geom-traits.h>
#include <geom_traits/units.h>
#include <icmesh/Mesh.h>
#include <iostream>

void test_1Dmesh(const std::size_t N) {
  auto eps = 1.e-7;
  compass::utils::Pretty_printer print;
  print("---------------------------------");
  print("-   1D mesh with", N + 1, "nodes  -");
  print("---------------------------------");
  // creates a 1D grid with N segments, N+1 nodes
  auto unit_step = icmesh::Constant_axis_offset{0., N, 1. / N};
  auto struct_1Dmesh = icmesh::mesh(icmesh::lattice(unit_step));

  for (auto &&P : struct_1Dmesh.vertices.span())
    print("Point (", P, ")");

  auto geom = geom::GeomProperties(struct_1Dmesh);
  auto edges_nodes =
      struct_1Dmesh.get_connectivity<struct_1Dmesh.edge, struct_1Dmesh.node>();
  for (auto &&nodes : edges_nodes.targets())
    print("edges_nodes ", nodes);

  auto &edge_center = geom.get_edge_centers(); // Vertex_field
  for (auto &&P : edge_center.span())
    print("Edge center (", P, ")");

  auto mesh_centers =
      geom.get_iset_centers(struct_1Dmesh.joined_elements); // Vertex_field
  for (auto &&P : mesh_centers.span())
    print("All mesh center (", P, ")");

  auto &edge_measure = geom.get_edge_measures(); // Scalar_field
  for (auto &&m : edge_measure.span())
    assert(abs(m - 1. / N) < eps);
}

void test_2Dmesh(const std::size_t N) {
  auto eps = 1.e-7;
  compass::utils::Pretty_printer print;
  print("------------------------------------------------");
  print("- 2D mesh with", N + 1, "nodes in each direction -");
  print("------------------------------------------------");
  // creates a 2D grid with (N segments, N+1 nodes) * (N segments, N+1 nodes)
  auto unit_step = icmesh::Constant_axis_offset{0., N, 1. / N};
  auto struct_2Dmesh = icmesh::mesh(icmesh::lattice(unit_step, unit_step));

  for (auto &&P : struct_2Dmesh.vertices.span())
    print("Point (", P, ")");

  auto geom = geom::GeomProperties(struct_2Dmesh);
  auto cells_nodes =
      struct_2Dmesh.get_connectivity<struct_2Dmesh.cell, struct_2Dmesh.node>();
  for (auto &&nodes : cells_nodes.targets())
    print("cells_nodes ", nodes);

  auto &edge_center = geom.get_edge_centers(); // Vertex_field
  for (auto &&P : edge_center.span())
    print("Edge center (", P, ")");

  auto &cell_center = geom.get_cell_centers(); // Vertex_field
  for (auto &&P : cell_center.span())
    print("Cell center (", P, ")");

  auto mesh_centers =
      geom.get_iset_centers(struct_2Dmesh.joined_elements); // Vertex_field
  for (auto &&P : mesh_centers.span())
    print("All mesh center (", P, ")");

  auto &edge_measure = geom.get_edge_measures(); // Scalar_field
  for (auto &&m : edge_measure.span())
    assert(abs(m - 1. / N) < eps);

  auto &cell_measure = geom.get_cell_measures(); // Scalar_field
  auto cell_vol = 1. / std::pow(N, 2);
  for (auto &&m : cell_measure.span())
    assert(abs(m - cell_vol) < eps);
}

void test_3Dmesh(const std::size_t N) {
  auto eps = 1.e-7;
  compass::utils::Pretty_printer print;
  print("------------------------------------------------");
  print("- 3D mesh with", N + 1, "nodes in each direction -");
  print("------------------------------------------------");
  // creates a 3D grid with (N segments, N+1 nodes) ** 3
  auto unit_step = icmesh::Constant_axis_offset{0., N, 1. / N};
  auto struct_3Dmesh =
      icmesh::mesh(icmesh::lattice(unit_step, unit_step, unit_step));

  for (auto &&P : struct_3Dmesh.vertices.span())
    print("Point (", P, ")");

  auto geom = geom::GeomProperties(struct_3Dmesh);
  auto cells_nodes =
      struct_3Dmesh.get_connectivity<struct_3Dmesh.cell, struct_3Dmesh.node>();
  for (auto &&nodes : cells_nodes.targets())
    print("cells_nodes ", nodes);

  auto &edge_center = geom.get_edge_centers(); // Vertex_field
  for (auto &&P : edge_center.span())
    print("Edge center (", P, ")");

  auto &face_center = geom.get_face_centers(); // Vertex_field
  for (auto &&P : face_center.span())
    print("Face center (", P, ")");

  auto &cell_center = geom.get_cell_centers(); // Vertex_field
  for (auto &&P : cell_center.span())
    print("Cell center (", P, ")");

  auto mesh_centers =
      geom.get_iset_centers(struct_3Dmesh.joined_elements); // Vertex_field
  for (auto &&P : mesh_centers.span())
    print("All mesh center (", P, ")");

  auto &edge_measure = geom.get_edge_measures(); // Scalar_field
  for (auto &&m : edge_measure.span())
    assert(abs(m - 1. / N) < eps);

  auto &face_measure = geom.get_face_measures(); // Scalar_field
  auto face_area = 1. / std::pow(N, 2);
  for (auto &&m : face_measure.span())
    assert(abs(m - face_area) < eps);

  auto &cell_measure = geom.get_cell_measures(); // Scalar_field
  auto cell_vol = 1. / std::pow(N, 3);
  for (auto &&m : cell_measure.span())
    assert(abs(m - cell_vol) < eps);

  // todo, how to use Vertex from geom ???
  using tmp_vertex = std::array<geom::Scalar, 3>;
  tmp_vertex x0{0.0, 1.0, 0.0};
  tmp_vertex x1{0.0, 0.0, 0.0};
  tmp_vertex x2{0.0, 0.0, 1.0};
  auto n_vect = geom::normal_vect<3>(x0, x1, x2);
  assert(abs(abs(n_vect[0]) - 1.0) < eps);
  assert(abs(n_vect[1]) < eps && abs(n_vect[2]) < eps);
  // face_unit_normal takes indices of nodes, it gets the coordinates from geom
  auto unit_normal = geom.template face_unit_normal<3, tmp_vertex>(
      std::array<int, 3>{0, 1, 3});
  assert(abs(abs(unit_normal[0]) - 1.0) < eps);
  assert(abs(unit_normal[1]) < eps && abs(unit_normal[2]) < eps);
  unit_normal = geom.template face_unit_normal<3, tmp_vertex>(
      std::array<int, 3>{4, 7, 10});
  assert(abs(unit_normal[0]) < eps && abs(unit_normal[1]) < eps);
  assert(abs(abs(unit_normal[2]) - 1.0) < eps);
}

int main() {

  test_1Dmesh(5);
  test_2Dmesh(3);
  test_3Dmesh(2);

  return 0;
}
