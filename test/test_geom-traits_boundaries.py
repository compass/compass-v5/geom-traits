from icmesh import regular_mesh
from geom_traits import (
    make_geom,
    grid_center,
    bottom_boundary_nodes,
    bottom_boundary_edges,
    bottom_boundary_faces,
    xmin_boundary_faces,
    all_boundaries_faces,
    bottom_boundary,
)


def test_boundaries():
    geom = make_geom(regular_mesh((5, 2, 3), extent=(1.5, 5.12, 3.2)))
    center = grid_center(geom)
    assert center == (0.75, 2.56, 1.6)
    vertices = geom.mesh.vertices.as_array()
    face_centers = geom.face_centers.as_array()
    bottom_bv = bottom_boundary_nodes(geom)
    left_bf = xmin_boundary_faces(geom)
    all_bf = all_boundaries_faces(geom)
    assert (vertices[bottom_bv.iset.mapping()][:, -1] <= geom.mesh.origin[-1]).all
    assert (face_centers[left_bf.iset.mapping()][:, 0] <= geom.mesh.origin[0]).all
    assert len(all_bf) == 62
    bottom_elemts = bottom_boundary(geom)
    bottom_be = bottom_boundary_edges(geom)
    bottom_bf = bottom_boundary_faces(geom)
    assert bottom_bv.issubset(bottom_elemts)
    assert bottom_be.issubset(bottom_elemts)
    assert bottom_bf.issubset(bottom_elemts)
    assert (bottom_bv | bottom_bf | bottom_be).issubset(bottom_elemts)
    assert (bottom_elemts).issubset(bottom_bv | bottom_bf | bottom_be)
    assert (bottom_bv | bottom_bf | bottom_be).issuperset(bottom_elemts)
    assert (bottom_elemts).issuperset(bottom_bv | bottom_bf | bottom_be)
