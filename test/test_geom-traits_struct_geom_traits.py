import numpy as np
from icmesh import regular_mesh, scottish_mesh
from geom_traits import (
    make_geom,
    StructGeomProperties_1,
    StructGeomProperties_2,
    StructGeomProperties_3,
)


def step(N, L):
    factor = 2 * L / N / (N + 1)
    return np.array([factor * (i + 1) for i in range(N)])


def check_cell_centers(geom):
    centers = geom.cell_centers
    other_centers = geom.get_iset_centers(geom.mesh.cells)
    assert (centers.as_array() == other_centers.as_array()).all()


def check_face_centers(geom):
    centers = geom.face_centers
    other_centers = geom.get_iset_centers(geom.mesh.faces)
    assert (centers.as_array() == other_centers.as_array()).all()


def check_cell_measures(geom):
    measures = geom.cell_measures
    other_measures = geom.get_iset_measures(geom.mesh.cells)
    assert (measures.as_array() == other_measures.as_array()).all()


def check_face_measures(geom):
    measures = geom.face_measures
    other_measures = geom.get_iset_measures(geom.mesh.faces)
    assert (measures.as_array() == other_measures.as_array()).all()


def test_1D_struct_geom_traits():

    Nz = 6
    Lz = 59.3

    print("--------------------------------------")
    print("2D regular grid")
    geom_prop = make_geom(regular_mesh((Nz,), extent=(Lz,)))
    same_geom_prop = StructGeomProperties_1(regular_mesh((Nz,), extent=(Lz,)))
    check_cell_centers(geom_prop)
    check_face_centers(geom_prop)
    check_cell_measures(geom_prop)
    check_face_measures(geom_prop)

    print("--------------------------------------")
    print("2D scottish grid")
    geom_prop = make_geom(scottish_mesh(step(Nz, Lz)))
    scot_mesh = geom_prop.mesh
    check_cell_centers(geom_prop)
    check_face_centers(geom_prop)
    check_cell_measures(geom_prop)
    check_face_measures(geom_prop)


def test_2D_struct_geom_traits():

    Nx = 5
    Nz = 6
    Lx = 23.15
    Lz = 59.3

    print("--------------------------------------")
    print("2D regular grid")
    geom_prop = make_geom(regular_mesh((Nx, Nz), extent=(Lx, Lz)))
    same_geom_prop = StructGeomProperties_2(regular_mesh((Nx, Nz), extent=(Lx, Lz)))
    check_cell_centers(geom_prop)
    check_face_centers(geom_prop)
    check_cell_measures(geom_prop)
    check_face_measures(geom_prop)

    print("--------------------------------------")
    print("2D scottish grid")
    geom_prop = make_geom(scottish_mesh(step(Nx, Lx), step(Nz, Lz)))
    scot_mesh = geom_prop.mesh
    check_cell_centers(geom_prop)
    check_face_centers(geom_prop)
    check_cell_measures(geom_prop)
    check_face_measures(geom_prop)


def test_3D_struct_geom_traits():

    Nx = 5
    Ny = 7
    Nz = 6
    Lx = 23.15
    Ly = 5.79
    Lz = 59.3

    print("--------------------------------------")
    print("3D regular grid")
    geom_prop = make_geom(regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz)))
    same_geom_prop = StructGeomProperties_3(
        regular_mesh((Nx, Ny, Nz), extent=(Lx, Ly, Lz))
    )
    check_cell_centers(geom_prop)
    check_face_centers(geom_prop)
    check_cell_measures(geom_prop)
    check_face_measures(geom_prop)

    print("--------------------------------------")
    print("3D scottish grid")
    geom_prop = make_geom(scottish_mesh(step(Nx, Lx), step(Ny, Ly), step(Nz, Lz)))
    check_cell_centers(geom_prop)
    check_face_centers(geom_prop)
    check_cell_measures(geom_prop)
    check_face_measures(geom_prop)
