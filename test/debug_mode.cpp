// because we use C assertions to test thing
// the NDEBUG macro must NOT be defined
// because if NDEBUG is defined as a macro name
// then assert does nothing
// cf. https://en.cppreference.com/w/cpp/error/assert

int main() {
#ifdef NDEBUG
  return -1;
#else
  return 0;
#endif
}
